class AddTokensToUserModel < ActiveRecord::Migration
  def change
  	add_column :users, :token, :text
  	add_column :users, :refresh_token, :text
  end

  def down
   remove_column :users, :token
   remove_column :users, :refresh_token
  end
end
