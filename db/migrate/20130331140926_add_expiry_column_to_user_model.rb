class AddExpiryColumnToUserModel < ActiveRecord::Migration
  def change
    add_column :users, :token_expiry, :datetime
  end

  def down
    remove_column :users, :token_expiry
  end
end
