class AddEmailColumnToUsers < ActiveRecord::Migration
  def change
  	add_column :users, :email, :text
  end

  def down
    remove_column :users, :email
  end
end
