require 'open-uri'
require 'nokogiri'
module Support::Contacts


  #Get Contacts From Gmail
  def getcontact
    client_id = ENV["google_key"]
    max_results = 3000
    url = "https://www.google.com/m8/feeds/contacts/default/full?client_id=#{client_id}&access_token=#{current_user.token}&max-results=#{max_results}";
    doc = Nokogiri::XML.parse(open(url))
    doc.xpath('//xmlns:feed/xmlns:entry[xmlns:title[node()]]')#returns non empty title nodes only
    rescue OpenURI::HTTPError => e
        if e.message == '401 Token invalid - Invalid token: Stateless token expired'
            refresh_token
            
        end
  end



  def refresh_token
    # Refresh auth token from google_oauth2.
      options = {
        body: {
        client_id: ENV["google_key"],
        client_secret: ENV["google_secret"],
        refresh_token: "#{current_user.refresh_token}",
        grant_type: "refresh_token"
      },
      headers: {
        'Content-Type' => 'application/x-www-form-urlencoded'

      }
    }

      refresh = HTTParty.post('https://accounts.google.com/o/oauth2/token', options)
        if refresh.code == 200
        current_user.token = refresh['access_token']
        current_user.save
      end
  end

  def update_contact
    update = HTTParty.puts('https://www.google.com/m8/feeds/contacts?userEmail=#{current_user.email/full/{contactId}')

  end

end

