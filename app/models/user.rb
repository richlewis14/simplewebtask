class User < ActiveRecord::Base
  attr_accessible :name, :provider, :uid, :email, :token, :refresh_token, :token_expiry, :expires_at

  #Validations


  #Methods
  def self.create_with_omniauth(auth)
  create! do |user|
    user.provider = auth["provider"]
    user.uid = auth["uid"]
    user.name = auth["info"]["name"]
    user.email = auth["info"]["email"]
    user.token = auth["credentials"]["token"]
    user.refresh_token = auth["credentials"]["refresh_token"]
    user.token_expiry = DateTime.now #+ 50.minutes
  end
end
end
