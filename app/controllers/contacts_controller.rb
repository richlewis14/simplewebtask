class ContactsController < ApplicationController
  require 'will_paginate/array'
  include Support::Contacts


  def index
    @mycontacts = getcontact.to_a.paginate(:page => params[:page], :per_page => 30)
    @mycontacts.sort!{|a, b| a.xpath('xmlns:title').text <=> b.xpath('xmlns:title').text}
  end

  def new

  end

  def create

  end

  def edit

  end

  def show
  

  end

  def update

  end

  def destroy

  end


end
