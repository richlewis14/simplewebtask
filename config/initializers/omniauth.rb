Rails.application.config.middleware.use OmniAuth::Builder do
  provider :google_oauth2, ENV['google_key'], ENV['google_secret'], { :scope => 'userinfo.email,https://www.google.com/m8/feeds/', :approval_prompt => "force"}
end